import object.Player;
import object.Warrior;
import object.wizard.FireWizard;
import object.wizard.HealerWizard;
import object.wizard.spell.Spell;
import util.BotUtil;
import util.UserUtil;

import java.util.ArrayList;
import java.util.List;

import static object.wizard.spell.SpellType.ATTACK;
import static object.wizard.spell.SpellType.HEAL;

public class Main {
    public static void main(String[] args) {
        UserUtil userUtil = new UserUtil();
        Player[] players = getPlayers();

        do {
            Player actualPlayer = UserUtil.selectPlayer(players);

            Player botPlayer = players[3];

            int turnNumber = 1;

            while (actualPlayer.isInLife && botPlayer.isInLife && turnNumber < 6) {
                System.out.println("Tour numéro " + turnNumber + " lancé");
                userUtil.launchPlayerTurn(actualPlayer, botPlayer);
                if (botPlayer.isInLife) {
                    BotUtil.launchBotTurn(actualPlayer, botPlayer);
                }
                turnNumber++;
            }

        } while (UserUtil.restartGame());
    }


    private static Player[] getPlayers() {
        Spell fireBall = new Spell("Fire Ball", 15, 25, ATTACK);
        Spell b = new Spell("bob", 15, 25, ATTACK);
        Spell c = new Spell("marley", 15, 25, ATTACK);
        Spell d = new Spell("fume", 15, 25, ATTACK);
        List<Spell> fireWizardSpells = new ArrayList<>(){{
            add(b);
            add(c);
            add(fireBall);
            add(d);
        }};

        Spell healingSpray = new Spell("healing Spray", 15, 25, HEAL);
        List<Spell> healerWizardSpells = new ArrayList<>(){{ add(healingSpray); }};

        HealerWizard healerWizard = new HealerWizard("Healer Wizard", 10, 100, 30, healerWizardSpells);
        FireWizard fireWizard = new FireWizard("fire Wizard", 10, 100, 30, fireWizardSpells);
        Warrior warrior = new Warrior("warrior", 10, 100);
        HealerWizard botHealer = new HealerWizard("Bot", 10, 100, 30, healerWizardSpells);
        return new Player[]{healerWizard, fireWizard, warrior, botHealer};
    }
}
