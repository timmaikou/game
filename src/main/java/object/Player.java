package object;

import java.util.Scanner;

public class Player {

    public String name;
    public boolean isInLife;
    public int attack;
    public int maximumHealthPoint;
    public int healthPoint;

    public static final String NEW_LINE = System.getProperty("line.separator");


    public Player(String name, int attack, int maximumHealthPoint) {
        this.name = name;
        this.attack = attack;
        this.maximumHealthPoint = maximumHealthPoint;
        this.healthPoint = maximumHealthPoint;
        this.isInLife = true;
    }

    public void attackPlayer(Player attackedPlayer) {

        if (attackedPlayer.getClass() == Warrior.class) {
            if (((Warrior) attackedPlayer).isShield) {
                System.out.println(this.name + " à tenté d'attaquer, " + attackedPlayer.name + " s'est protégé " + NEW_LINE);
            } else {
                attackedPlayer.healthPoint = attackedPlayer.healthPoint - this.attack;
            }

        } else {
            attackedPlayer.healthPoint = attackedPlayer.healthPoint - this.attack;
        }

        if (attackedPlayer.healthPoint <= 0) {
            attackedPlayer.isInLife = false;
            System.out.println(attackedPlayer.name + " EST MORT");
        } else {
            System.out.println(
                    "   " + this.name + " attaque " + attackedPlayer.name + NEW_LINE +
                            "   PV " + attackedPlayer.name + " = " + attackedPlayer.healthPoint + NEW_LINE);
        }
    }

    public void displayTurn() {
        System.out.println(NEW_LINE + "TOUR DE : " + this.name);
    }

    public void displayActionMenu() {
        System.out.println(
                "   1 = Attaquer" + NEW_LINE +
                        "   2 = Passer son Tour");
    }


    public void launchAction(int userChoice, Player attackedPlayer) {
        switch (userChoice) {
            case 1 -> this.attackPlayer(attackedPlayer);
            case 2 -> System.out.println("VOUS AVEZ PASSE VOTRE TOUR");
            default -> System.out.println("Cette valeur n'est pas prise en compte. Veuillez reesayer ...");
        }
    }

    public static int choiceUser() {
        Scanner myScanner = new Scanner(System.in);
        return myScanner.nextInt();
    }

    @Override
    public String toString() {
        return "NOM : " + name + NEW_LINE +
                "         attaque " + attack +
                ", PV " + maximumHealthPoint;
    }
}


