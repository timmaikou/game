package object;

public class Warrior extends Player {

    boolean isShield;

    public Warrior(String name, int attack, int healthPointMaximum) {
        super(name, attack, healthPointMaximum);
        this.isShield = false;
    }

    public void shieldHimSelf() {
        isShield = true;
        System.out.println(this.name + " c'est protegé");
    }

    public void breakShield() {
        isShield = false;
    }

    @Override
    public void displayActionMenu() {
        System.out.println("1 : attaquer" + NEW_LINE +
                "2 : se protéger" + NEW_LINE +
                "3 : passer son tour");
    }

    @Override
    public void launchAction(int userChoice, Player attackedPlayer) {
        switch (userChoice) {
            case 1 -> this.attackPlayer(attackedPlayer);
            case 2 -> this.shieldHimSelf();
            case 3 -> System.out.println("VOUS AVEZ PASSE VOTRE TOUR");
            default -> System.out.println("Cette valeur n'est pas prise en compte. Veuillez reesayer ...");
        }
    }
}
