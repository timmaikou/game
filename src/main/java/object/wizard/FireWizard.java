package object.wizard;


import object.wizard.spell.Spell;

import java.util.List;

public class FireWizard extends Wizard {

    public FireWizard(String name, int attack, int healthPointMaximum, int maximumManaPoint, List<Spell> spells) {
        super(name, attack, healthPointMaximum, maximumManaPoint, spells);
    }
}

