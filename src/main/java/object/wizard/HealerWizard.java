package object.wizard;


import object.wizard.spell.Spell;

import java.util.List;

public class HealerWizard extends Wizard {

    public HealerWizard(String name, int attack, int healthPointMaximum, int maximumManaPoint, List<Spell> spells) {
        super(name, attack, healthPointMaximum, maximumManaPoint, spells);
    }

    public void healHimSelf() {
        Spell healingSpray = getSpell("healing Spray");
        if (this.manaPoint >= healingSpray.requiredMana) {
            if (healthPoint >= maximumHealthPoint - healingSpray.power) {
                this.healthPoint = this.maximumHealthPoint;
            } else {
                this.healthPoint = this.healthPoint + healingSpray.power;
            }

            System.out.println("   " + this.name + " c'est soigné." + NEW_LINE +
                    "   PV " + this.name + " : " + this.healthPoint);
            manaPoint = manaPoint - healingSpray.requiredMana;

        } else {
            System.out.println("Mana requit insufisante");
        }
    }
}