package object.wizard;


import object.Player;
import object.wizard.spell.Spell;

import java.util.List;

public class Wizard extends Player {

    int maximumManaPoint;
    int manaPoint;
    List<Spell> spells;

    public Wizard(String name, int attack, int healthPointMaximum, int maximumManaPoint, List<Spell> spells) {
        super(name, attack, healthPointMaximum);
        this.maximumManaPoint = maximumManaPoint;
        this.manaPoint = maximumManaPoint;
        this.spells = spells;
    }

    protected void showSpell(Spell spellToShow){
        System.out.println(
                spellToShow.name + "(power : " + spellToShow.power + "/required Mana : " + spellToShow.requiredMana + ")"
                );
    }

    protected void showSpells(){
        for (int i = 0; i < this.spells.size(); i ++){
            Spell spellToShow = this.spells.get(i);
            System.out.println("i) ");showSpell(spellToShow);
    }
}

    @Override
    public void displayTurn() {
        super.displayTurn();
        if (this.manaPoint > this.maximumManaPoint - 5) {
            this.manaPoint = this.maximumManaPoint;
        } else {
            this.manaPoint = this.manaPoint + 5;
        }
        System.out.println("   mana: " + manaPoint + ", PV : " + this.healthPoint);
    }

    @Override
    public String toString() {
        String playerInfo = super.toString();
        return playerInfo + ", Mana : " + manaPoint + NEW_LINE +
                " Sort : " + this.spells + NEW_LINE + this.spells.toString();
    }

    @Override
    public void displayActionMenu() {
        System.out.println(
                "   1 = Attaquer" + NEW_LINE +
                "   2 = Utiliser un pouvoir " + NEW_LINE +
                "   3 = Passer son Tour");
    }

    @Override
    public void launchAction(int userChoice, Player attackedPlayer) {
        switch (userChoice){
            case 1 : this.attackPlayer(attackedPlayer);
            case 2 : useSpell(attackedPlayer);
            case 3 : System.out.println("Vous avez passé votre tour ...");
        }
    }


    public void useSpell(Player attackedPlayer){
        showSpells();
        int userChoice = choiceUser();
        Spell spellToLaunch = spells.get(userChoice);
        launchSpell(spellToLaunch, attackedPlayer);
    }

    public void launchSpell(Spell spellToLaunch, Player attackedPlayer){
        switch (spellToLaunch.spellType){
            case ATTACK:
                if (this.manaPoint <= spellToLaunch.requiredMana) {
                    System.out.println("Mana insufisante.");
                } else {
                    attackedPlayer.healthPoint = attackedPlayer.healthPoint - spellToLaunch.power;
                    this.manaPoint = this.manaPoint - spellToLaunch.requiredMana;
                }
            case HEAL:
                    if (this.manaPoint >= spellToLaunch.requiredMana) {
                    if (healthPoint >= maximumHealthPoint - spellToLaunch.power) {
                        this.healthPoint = this.maximumHealthPoint;
                    } else {
                        this.healthPoint = this.healthPoint + spellToLaunch.power;
                    }

                    System.out.println("   " + this.name + " c'est soigné." + NEW_LINE +
                            "   PV " + this.name + " : " + this.healthPoint);
                    manaPoint = manaPoint - spellToLaunch.requiredMana;

                } else {
                        System.out.println("Mana requit insufisante");
                    }
        }
    }

    protected Spell getSpell(String nameOfSpell) {
        Spell requiredSpell = null;
        for (int i = 0; i < spells.size(); i++) {
            Spell spell = spells.get(i);
            if (spell.name.equals(nameOfSpell)) {
                requiredSpell = spell;
            }
        }
        return requiredSpell;
    }
}
