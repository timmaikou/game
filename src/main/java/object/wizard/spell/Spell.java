package object.wizard.spell;

public class Spell {
    public String name;
    public int requiredMana;
    public int power;
    public SpellType spellType;

    public Spell(String name, int requiredMana, int power, SpellType spellType) {
        this.name = name;
        this.requiredMana = requiredMana;
        this.power = power;
        this.spellType = spellType;
    }

    public static final String NEW_LINE = System.getProperty("line.separator");

    public Spell() {
        name = "";
        power = 0;
        requiredMana = 0;
        spellType = SpellType.ATTACK;
    }

    @Override
    public String toString() {
        return  "      -requiredMana =" + requiredMana + NEW_LINE +
                "      -power =" + power;
    }
}
