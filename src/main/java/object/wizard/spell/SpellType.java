package object.wizard.spell;

public enum SpellType {
    ATTACK,
    HEAL,
    BUFF
}
