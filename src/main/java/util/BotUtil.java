package util;


import object.Player;

public class BotUtil {

    public static void launchBotTurn(Player actualPlayer, Player botPlayer) {
        botPlayer.displayTurn();
        botPlayer.attackPlayer(actualPlayer);
        if(actualPlayer.healthPoint <= 0){
            System.out.println("Le BOT vous a tuer !");
        }
        else {
            System.out.println("Le bot vous a attaqué !");
        }
    }
}

