package util;


import object.Player;

import static object.Player.choiceUser;

public class UserUtil {


    private static final String NEW_LINE = System.getProperty("line.separator");


    public void launchPlayerTurn(Player actualPlayer, Player botPlayer) {

        actualPlayer.displayTurn();
        int userChoice;
        do {
            actualPlayer.displayActionMenu();
            userChoice = choiceUser();
            actualPlayer.launchAction(userChoice, botPlayer);

        } while (userChoice > 3 || userChoice < 1);
    }

    public static Player selectPlayer(Player[] players) {
        int selectPlayer;
        do {
            displaySelectPlayerMenu(players);
            selectPlayer = choiceUser();
        } while (selectPlayer > 3);

        Player actualPlayer;

        switch (selectPlayer) {
            case 1 -> actualPlayer = players[0];
            case 2 -> actualPlayer = players[1];
            default -> actualPlayer = players[2];
        }
        System.out.println("Tu es " + actualPlayer.name);
        return actualPlayer;
    }



    private static void displaySelectPlayerMenu(Player[] players) {
        System.out.println(
                "Select your champion" + NEW_LINE +
                        "1 = " + players[0].toString() + NEW_LINE +
                        "2 = " + players[1].toString() + NEW_LINE +
                        "3 = " + players[2].toString() + NEW_LINE);
    }

    public static boolean restartGame() {
        System.out.println("Si vous voulez rejouer cliquer '1'");
        int action = choiceUser();
        boolean restartGame = false;
        if (action == 1) {
            restartGame = true;
        }
        return restartGame;
    }

}
